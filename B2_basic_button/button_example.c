/*
 * Copyright (c) 2020 Nanjing Xiaoxiongpai Intelligent Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
// #include "iot_gpio.h"
#include "hi_gpio.h"
#include "hi_io.h"
#include <hi_adc.h>
// #include "wifiiot_gpio.h"
// #include "wifiiot_gpio_ex.h"

#define SWITCH_GPIO HI_GPIO_IDX_5
#define LED_GPIO 9

#define     ADC_TEST_LENGTH             (20)
#define     VLT_MIN                     (100)

hi_u16  g_adc_buf[ADC_TEST_LENGTH] = { 0 };
hi_u16  g_gpio5_adc_buf[ADC_TEST_LENGTH] = { 0 };

int g_ledState = HI_GPIO_VALUE0;

static void F1_Pressed(void)
{
    if (g_ledState == HI_GPIO_VALUE1)
    {
        hi_gpio_set_ouput_val(LED_GPIO, HI_GPIO_VALUE0);
        g_ledState = HI_GPIO_VALUE0;
    }
}
static void F2_Pressed(void)
{
    if (g_ledState == HI_GPIO_VALUE0) {
        hi_gpio_set_ouput_val(LED_GPIO, HI_GPIO_VALUE1);
        g_ledState = HI_GPIO_VALUE1;
    }
}

float get_gpio5_voltage(void)
{
    int i;
    hi_u16 data;
    hi_u32 ret;
    hi_u16 vlt;
    float voltage;
    float vlt_max = 0;
    float vlt_min = VLT_MIN;

    memset_s(g_gpio5_adc_buf, sizeof(g_gpio5_adc_buf), 0x0, sizeof(g_gpio5_adc_buf));
    for (i = 0; i < ADC_TEST_LENGTH; i++) {
        ret = hi_adc_read(HI_ADC_CHANNEL_2, &data, HI_ADC_EQU_MODEL_4, HI_ADC_CUR_BAIS_DEFAULT, 0xF0); //ADC_Channal_2  自动识别模式  CNcomment:4次平均算法模式 CNend */
        if (ret != HI_ERR_SUCCESS) {
            printf("ADC Read Fail\n");
            return  0.0;
        }    
        g_gpio5_adc_buf[i] = data;
    }

    for (i = 0; i < ADC_TEST_LENGTH; i++) {  
        vlt = g_gpio5_adc_buf[i]; 
        voltage = (float)vlt * 1.8 * 4 / 4096.0;  /* vlt * 1.8* 4 / 4096.0为将码字转换为电压 */
        vlt_max = (voltage > vlt_max) ? voltage : vlt_max;
        vlt_min = (voltage < vlt_min) ? voltage : vlt_min;
    }
    return vlt_max;
}

 void* button_pressed(char *arg)
 {
    (void)arg;
    float vlt = get_gpio5_voltage();
    if (vlt >= 0.6 && vlt < 1.0)
    {
        F1_Pressed();
    }
    else if (vlt >= 1.0)
    {
        F2_Pressed();
    }
 };

static void ButtonExampleEntry(void)
{
    // GpioInit();
    hi_gpio_init();

    //初始化LED灯
    // IoSetFunc(WIFI_IOT_IO_NAME_GPIO_2, WIFI_IOT_IO_FUNC_GPIO_2_GPIO);

    // GpioSetDir(WIFI_IOT_IO_NAME_GPIO_2, WIFI_IOT_GPIO_DIR_OUT);
    hi_gpio_set_dir(LED_GPIO, HI_GPIO_DIR_OUT);

    //初始化SWITCH，设置为下降沿触发中断
    // IoSetFunc(WIFI_IOT_IO_NAME_GPIO_11, WIFI_IOT_IO_FUNC_GPIO_11_GPIO);
    hi_io_set_func(SWITCH_GPIO, HI_IO_FUNC_GPIO_5_GPIO);

    // GpioSetDir(WIFI_IOT_IO_NAME_GPIO_11, WIFI_IOT_GPIO_DIR_IN);
    // hi_gpio_set_dir(HI_GPIO_IDX_5, HI_GPIO_DIR_IN);
    hi_gpio_set_dir(SWITCH_GPIO, HI_GPIO_DIR_IN);
    // IoSetPull(WIFI_IOT_IO_NAME_GPIO_11, WIFI_IOT_IO_PULL_UP);
    hi_io_set_pull(SWITCH_GPIO, HI_IO_PULL_UP);
    // GpioRegisterIsrFunc(WIFI_IOT_IO_NAME_GPIO_11, WIFI_IOT_INT_TYPE_EDGE, WIFI_IOT_GPIO_EDGE_FALL_LEVEL_LOW, F1_Pressed, NULL);
    hi_gpio_register_isr_function(SWITCH_GPIO, HI_INT_TYPE_EDGE, HI_GPIO_EDGE_FALL_LEVEL_LOW, button_pressed, NULL);


    //初始化F2按键，设置为下降沿触发中断
    // IoSetFunc(WIFI_IOT_IO_NAME_GPIO_12, WIFI_IOT_IO_FUNC_GPIO_12_GPIO);

    // GpioSetDir(WIFI_IOT_IO_NAME_GPIO_12, WIFI_IOT_GPIO_DIR_IN);
    // IoSetPull(WIFI_IOT_IO_NAME_GPIO_12, WIFI_IOT_IO_PULL_UP);

    // GpioRegisterIsrFunc(WIFI_IOT_IO_NAME_GPIO_12, WIFI_IOT_INT_TYPE_EDGE, WIFI_IOT_GPIO_EDGE_FALL_LEVEL_LOW, F2_Pressed, NULL);
}

APP_FEATURE_INIT(ButtonExampleEntry);