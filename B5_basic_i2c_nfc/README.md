# BearPi-HM_Nano开发板基础外设开发——I2C控制NFC芯片
本示例将演示如何在BearPi-HM_Nano开发板上使用I2C协议向NFC芯片写入数据

## I2C API分析
本示例主要使用了以下API完成I2C采样的功能
## I2cInit()
```c
unsigned int hi_i2c_init(hi_i2c_idx id, hi_u32 baudrate)
```
 **描述：**

用指定的频率初始化I2C设备


**参数：**

|名字|描述|
|:--|:------| 
| id | I2C设备ID.  |
| baudrate |I2C频率|

## I2cSetBaudrate()
```c
unsigned int hi_i2c_set_baudrate(hi_i2c_idx id, hi_u32 baudrate)
```
 **描述：**

为I2C设备设置频率


**参数：**

|名字|描述|
|:--|:------| 
| id | I2C设备ID.  |
| baudrate |I2C频率|

## hi_i2c_write()
```c
hi_u32  hi_i2c_write(hi_i2c_idx id, hi_u16 device_addr, const hi_i2c_data *i2c_data)
```
 **描述：**

将数据写入I2C设备


**参数：**

|名字|描述|
|:--|:------| 
| id | I2C设备ID.  |
| deviceAddr |I2C设备地址|
| i2cData |表示写入的数据|

## hi_i2c_read()
```c
hi_u32 hi_i2c_read(hi_i2c_idx id, hi_u16 device_addr, const hi_i2c_data *i2c_data)
```
 **描述：**

从I2C设备读取数据。读取的数据将保存到i2cData指定的地址


**参数：**

|名字|描述|
|:--|:------| 
| id | I2C设备ID.  |
| deviceAddr |I2C设备地址|
| i2cData |表示要读取的数据指向的指针|



## 硬件设计
如下图所示，NFC芯片使用的是I2C协议，I2C_SCL与GPIO_14相连接，I2C_SDA与GPIO_13相连接，所以需要编写软件使用GPIO_13和GPIO_14产生I2C信号去控制NFC芯片

## 软件设计

**主要代码分析**

代码移植自liteos的HiStreaming项目,加入 #include "ohos_init.h" 和 APP_FEATURE_INIT(app_c08i_nfc_i2c_demo_task); 完成ohos的兼容。
同时nfc部分加入了对ndef发送带chaining标志的连续帧的支持。

这部分代码为I2C初始化的代码，首先用 `hi_io_set_func()` 函数将GPIO_14复用为I2C0_SDA，GPIO_14复用为I2C0_SCL。然后调用hi_i2c_init()函数初始化I2C0端口，最后使用 `hi_i2c_set_baudrate()` 函数设置I2C1的频率为400kbps.
```c
// in app_io_init.c
hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);

hi_i2c_init(HI_I2C_IDX_0, 400000);//baud 400k
hi_i2c_set_baudrate(HI_I2C_IDX_0, 400000);
```

## 编译调试

### 修改 BUILD.gn 文件


修改 `applications\BearPi\BearPi-HM_Nano\sample` 路径下 BUILD.gn 文件，指定 `i2c_example` 参与编译。

```r
#"B1_basic_led_blink:led_example",
#"B2_basic_button:button_example",
#"B3_basic_pwm_led:pwm_example",
#"B4_basic_adc:adc_example",
"B5_basic_i2c_nfc:i2c_example",
#"B6_basic_uart:uart_example",
```   

    


### 运行结果<a name="section18115713118"></a>

示例代码编译烧录代码后，按下开发板的RESET按键，通过串口助手查看日志，并请使用带有NFC功能的手机靠近开发板，读取nfc标签或者写入ndef标签,并查看console的log
