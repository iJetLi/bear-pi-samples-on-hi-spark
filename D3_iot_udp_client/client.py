from socket import *

HOST = '192.168.3.118'
PORT = 8888
BUFSIZ = 1024
ADDR = (HOST,PORT)

udpCliSock = socket(AF_INET,SOCK_DGRAM)

while True:
    data = raw_input('>')
    msg = data.encode('utf-8')

    # for data in [b'Michael', b'Tracy', b'Sarah']:
    udpCliSock.sendto(msg, (ADDR))
    print(udpCliSock.recv(BUFSIZ).decode('utf-8'))

udpCliSock.close()


