from socket import *
from time import ctime

HOST = ''
PORT = 8888
BUFSIZ = 1024
ADDR = (HOST,PORT)

udpSerSock = socket(AF_INET,SOCK_DGRAM)
udpSerSock.bind(ADDR)

while True:
    data, addr = udpSerSock.recvfrom(BUFSIZ)
    print('Received from %s:%s.' % addr )
    print(data)
    reply = 'Hello, %s!' % data.decode('utf-8')
    udpSerSock.sendto(reply.encode('utf-8'), addr)
